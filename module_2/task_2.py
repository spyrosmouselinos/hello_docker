def task_2() -> str:
    try:
        with open('/var/module_configs/module_2.cfg', 'r') as fin:
            module_2_config = fin.read()
    except FileNotFoundError:
        module_2_config = None

    return f"Hello from Task 2 Sofia!\nModule Config Contained{module_2_config}\n"
