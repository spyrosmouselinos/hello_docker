def task_1() -> str:
    try:
        with open('/var/module_configs/module_1.cfg', 'r') as fin:
            module_1_config = fin.read()
    except FileNotFoundError:
        module_1_config = None

    return f"Hello from Task 1 Sofia!\nModule Config Contained{module_1_config}\n"
