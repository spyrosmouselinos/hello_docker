from flask import Flask, request
from module_1.task_1 import task_1
from module_2.task_2 import task_2

app = Flask(__name__)
app.url_map.strict_slashes = False


class Controller(object):
    def __init__(self):
        return

    @staticmethod
    @app.route("/test/<module>")
    def test_controller(module):
        """
        Simple method to test if controller works
        :return: confirmation text
        """

        if request.method == 'GET':
            if int(module) == 1:
                return task_1()
            else:
                return task_2()
