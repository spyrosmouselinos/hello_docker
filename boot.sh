#!/bin/sh
gunicorn --workers 1 --max-requests 10 --error-logfile /var/log/hello_docker/err.log --capture-output -t 600 wsgi:app --bind 0.0.0.0:5000